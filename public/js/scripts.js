$(function() {
    var btn_movil = $('#nav-mobile'),
        menu = $('#menu').find('ul');
    // Al dar click agregar/quitar clases que permiten el despliegue del menú
    btn_movil.on('click', function (e) {
        e.preventDefault();
        var el = $(this);
        el.toggleClass('nav-active');
        menu.toggleClass('open-menu');
    })
});

$(document).ready(function() {
  $('.click_mobile').click(function(e) { 
    menu = $('#menu').find('ul'); 
    menu.toggleClass('open-menu');
  });
});

(function($){
  $(document).on('contextmenu', 'img', function() {
      return false;
  })
})(jQuery);

$('img').on('dragstart', function(event) { event.preventDefault(); });